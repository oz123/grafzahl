# Build the manager binary
FROM golang:1.17 as builder

WORKDIR /workspace
# Copy the Go Modules manifests
COPY go.mod go.mod
COPY go.sum go.sum
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN go mod download -x

# Copy the go source
COPY cmd/ cmd/
COPY pkg/ pkg/

ARG VERSION

RUN \
    # Check for mandatory build arguments
    : "${VERSION:?Build argument VERSION needs to be set and non-empty.}"

# Build
RUN CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64 \
    go build \
    -ldflags "-X gitlab.com/oz123/grafzahl/pkg/operator.VERSION=$VERSION" \
    -a -o grafzahl cmd/main.go

# Use distroless as minimal base image to package the manager binary
# Refer to https://github.com/GoogleContainerTools/distroless for more details
FROM gcr.io/distroless/static:nonroot
WORKDIR /
COPY --from=builder /workspace/grafzahl .

ENTRYPOINT ["/grafzahl"]
