VERSION ?= $(shell git describe --always) 
GOOS    ?= $(shell go env | grep GOOS | cut -d'"' -f2)
BINARY  := grafzahl

LDFLAGS := -X gitlab.com/oz123/grafzahl/pkg/operator.VERSION=$(VERSION)
GOFLAGS := -ldflags "$(LDFLAGS)"

SRCDIRS  := cmd pkg
PACKAGES := $(shell find $(SRCDIRS) -type d)
GOFILES  := $(addsuffix /*.go,$(PACKAGES))
GOFILES  := $(wildcard $(GOFILES))
REGISTRY := docker.io
ORG      := oz123
PROJ     := grafzahl
IMG      := $(REGISTRY)/$(ORG)/$(PROJ):$(VERSION)

.PHONY: all clean

all: bin/$(GOOS)/$(BINARY)

bin/%/$(BINARY): $(GOFILES) Makefile
	GOARCH=amd64 go build $(GOFLAGS) -v -o $(BINARY) cmd/main.go

.PHONY: vendor
vendor:
	@go mod vendor

.PHONY: codegen
codegen: ### Generate code
	bash ./codegen/codegen.sh


.PHONY: docker-build
docker-build: ## Build docker image with the operator.
	docker build --build-arg VERSION=$(VERSION) -t $(IMG) .


