package operator

import (
	"context"
	"fmt"
	"log"

	myclientset "gitlab.com/oz123/grafzahl/pkg/generated/clientset/versioned"
	appsv1 "k8s.io/api/apps/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	types "k8s.io/apimachinery/pkg/types"
	kubeinformers "k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/cache"
)

var GrafzahlPatch = `[{"op": "replace", "path": "/spec/%ss", "value": %d }]`
var GrafzahlLabel = `{"metadata": {"labels": {"grafzahl": "counted"}}}`

type deploymentWatcher struct {
	deploymentInformer cache.SharedIndexInformer
	clientset          *kubernetes.Clientset
	gzClientSet        *myclientset.Clientset
}

func (d *deploymentWatcher) patchDeployment(deployment *appsv1.Deployment) {

	updated, err := d.clientset.AppsV1().Deployments(deployment.Namespace).Patch(
		context.TODO(), deployment.Name, types.MergePatchType, []byte(GrafzahlLabel), metav1.PatchOptions{})

	if err != nil {
		log.Printf("Error: %s\nUpdated: %s", err, updated)
	}
	log.Printf("Updated labels for %s/%s", deployment.Namespace, deployment.Name)
}

func (d *deploymentWatcher) patchNumbers() {
	gz := d.gzClientSet.GrafzahlV1beta1()

	total, err := gz.Numbers().Get(context.TODO(), "total", metav1.GetOptions{})

	if err != nil {
		log.Printf("Error could not get total: %s\n", err)
	}

	log.Printf("Total deployments: %d", total.Spec.Deployments)
	total.Spec.Deployments = total.Spec.Deployments + 1

	result, err := gz.Numbers().Update(
		context.TODO(), total, metav1.UpdateOptions{})
	if err != nil {
		log.Printf("Error patching: %s\n", err)
	}
	log.Printf("Updated deployments: %d", result.Spec.Deployments)

}

func (d *deploymentWatcher) onAddDeployment(obj interface{}) {
	deployment := obj.(*appsv1.Deployment)
	fmt.Printf("deployment added: %s/%s\n", deployment.Namespace, deployment.Name)
	d.patchNumbers()
	d.patchDeployment(deployment)

}

func newDeploymentWatcher(client *kubernetes.Clientset, gzClient *myclientset.Clientset) *deploymentWatcher {

	labelOptions := kubeinformers.WithTweakListOptions(func(opts *metav1.ListOptions) {
		opts.LabelSelector = "grafzahl!=counted"
	})

	kubeInformerFactory := kubeinformers.NewSharedInformerFactoryWithOptions(client,
		defaultResyncInterval, labelOptions)
	deploymentInformer := kubeInformerFactory.Apps().V1().Deployments().Informer()

	dw := &deploymentWatcher{deploymentInformer: deploymentInformer,
		clientset:   client,
		gzClientSet: gzClient,
	}

	deploymentInformer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: dw.onAddDeployment,
		DeleteFunc: func(obj interface{}) {
			//fmt.Printf("deployment deleted: %s \n", obj)
		},
		UpdateFunc: func(oldObj, newObj interface{}) {
			//fmt.Printf("deployment updated: %s \n", newObj)
		},
	})

	return dw
}

func (d *deploymentWatcher) run(stopChan <-chan struct{}) {
	d.deploymentInformer.Run(stopChan)
}
