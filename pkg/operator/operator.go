package operator

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	gzv1beta1 "gitlab.com/oz123/grafzahl/pkg/apis/grafzahl/v1beta1"
	myclientset "gitlab.com/oz123/grafzahl/pkg/generated/clientset/versioned"
	myscheme "gitlab.com/oz123/grafzahl/pkg/generated/clientset/versioned/scheme"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	typedcorev1 "k8s.io/client-go/kubernetes/typed/core/v1"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/tools/record"
)

var (
	VERSION               = "0.0.0.dev"
	defaultResyncInterval = time.Second * 10
)

const (
	GrafzahlStart           = "Grafzahl Started"
	GrafzahlPodAdded        = "Grafzahl found another Pod"
	GrafzahlDeploymentAdded = "Grafzahl found another Deployments"
)

type Options struct {
	KubeConfig string
}

type Grafzahl struct {
	Options
	clientset         *kubernetes.Clientset
	gzClientSet       *myclientset.Clientset
	podwatcher        *podWatcher
	deploymentwatcher *deploymentWatcher
	eventswatcher     *eventsWatcher
	recorder          record.EventRecorder
}

func New(options Options) (*Grafzahl, error) {
	config, err := newClientConfig(options)
	if err != nil {
		return nil, fmt.Errorf("config: %w", err)
	}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, fmt.Errorf("clientset: %w", err)
	}

	gzClientSet, err := myclientset.NewForConfig(config)
	if err != nil {
		return nil, fmt.Errorf("myclientset: %w", err)
	}

	eventBroadcaster := record.NewBroadcaster()
	eventBroadcaster.StartStructuredLogging(0)
	eventBroadcaster.StartRecordingToSink(&typedcorev1.EventSinkImpl{Interface: clientset.CoreV1().Events("")})

	gz := &Grafzahl{
		Options:           options,
		clientset:         clientset,
		gzClientSet:       gzClientSet,
		podwatcher:        newPodWatcher(clientset),
		deploymentwatcher: newDeploymentWatcher(clientset, gzClientSet),
		eventswatcher:     newEventsWatcher(clientset, gzClientSet),
		recorder:          eventBroadcaster.NewRecorder(myscheme.Scheme, corev1.EventSource{Component: "GrafzahlZahl"}),
	}

	return gz, nil
}

func (gz *Grafzahl) Run(ctx context.Context) error {
	log.Printf("Grafzahl %v is ready to count \n", VERSION)

	gz.initGrafzahl(ctx)

	stopChan := make(chan struct{})

	go gz.podwatcher.run(stopChan)
	go gz.deploymentwatcher.run(stopChan)
	go gz.eventswatcher.run(stopChan)

	numbers, err := gz.gzClientSet.GrafzahlV1beta1().Numbers().Get(context.TODO(), "total", metav1.GetOptions{})

	if err != nil {
		fmt.Printf("Error %s\n", err)
	} else {
		msg := fmt.Sprintf("Deployments: %d, Pods: %d\n", numbers.Spec.Deployments, numbers.Spec.Pods)
		log.Printf(msg)
		gz.recorder.Event(numbers, corev1.EventTypeNormal, GrafzahlStart, msg)
	}

	select {
	case <-ctx.Done():
		close(stopChan)
		break
	}
	return nil
}

func (gz *Grafzahl) initGrafzahl(ctx context.Context) error {
	//var dr DeleteResponse
	deployments, errd := gz.clientset.AppsV1().Deployments("").List(context.TODO(), metav1.ListOptions{})
	if errd != nil {
		return errd
	}
	pods, err := gz.clientset.CoreV1().Pods("").List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		return err
	}

	nPods, nDeployments := len(pods.Items), len(deployments.Items)

	fmt.Printf("There are %d pods in the cluster\n", nPods)
	fmt.Printf("There are %d deployments in the cluster\n", nDeployments)

	err = gz.gzClientSet.GrafzahlV1beta1().Numbers().Delete(context.TODO(), "total", metav1.DeleteOptions{})

	if err != nil && !strings.Contains(fmt.Sprint(err), "not found") {
		log.Printf("Error deleting: %+v\n", err)
	}
	number := gzv1beta1.Number{
		ObjectMeta: metav1.ObjectMeta{
			Name: "total",
		},
	}
	number.Spec.Deployments = int32(nDeployments)
	number.Spec.Pods = int32(nPods)

	_, err = gz.gzClientSet.GrafzahlV1beta1().Numbers().Create(context.TODO(), &number, metav1.CreateOptions{})

	if err != nil {
		log.Printf("Error creating: %+v\n", err)
	}

	log.Printf("Initialized total deployments: %d, pods: %d\n", nDeployments, nPods)

	return nil
}

func newClientConfig(options Options) (*rest.Config, error) {

	defaultRules := clientcmd.NewDefaultClientConfigLoadingRules()

	if options.KubeConfig != "" {
		defaultRules.ExplicitPath = options.KubeConfig
	}

	config, err := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(defaultRules, nil).ClientConfig()
	if err != nil {
		log.Printf("Error loading the config file, trying in cluster config ...")
		config, err = rest.InClusterConfig()
		if err != nil {
			return nil, fmt.Errorf("Creating in-cluster config failed: %w", err)
		}
	}
	return config, nil

}
