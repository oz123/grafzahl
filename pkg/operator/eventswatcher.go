package operator

import (
	"context"
	"fmt"
	"log"

	myclientset "gitlab.com/oz123/grafzahl/pkg/generated/clientset/versioned"
	appsv1 "k8s.io/api/apps/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/watch"
	"k8s.io/client-go/kubernetes"
)

type eventsWatcher struct {
	eventsInformer watch.Interface
	clientset      *kubernetes.Clientset
	gzClientSet    *myclientset.Clientset
}

func newEventsWatcher(client *kubernetes.Clientset, gzClient *myclientset.Clientset) *eventsWatcher {
	// See the real implementation of Watch here
	// https: //github.com/kubernetes/client-go/blob/4ae0d1917adff2d93ea4c6a7ae3a9e5c19280b9f/rest/request.go#L671
	watcher, err := client.AppsV1().Deployments("").Watch(context.TODO(), metav1.ListOptions{})
	if err != nil {
		log.Fatal(err)
	}
	ew := eventsWatcher{
		eventsInformer: watcher,
		clientset:      client,
		gzClientSet:    gzClient,
	}
	return &ew
}

func (e *eventsWatcher) run(stopChan <-chan struct{}) {

	for event := range e.eventsInformer.ResultChan() {
		deployment := event.Object.(*appsv1.Deployment)
		switch event.Type {
		case watch.Added:
			fmt.Printf("Deployment %s/%s Replicas %d\n", deployment.Namespace, deployment.Name, *deployment.Spec.Replicas)
		case watch.Modified:
			log.Printf("Deployment %s/%s: Replicas %d Ready %d\n", deployment.Namespace, deployment.Name, *deployment.Spec.Replicas, deployment.Status.ReadyReplicas)
		case watch.Deleted:
			log.Printf("Deleted %s", event.Object)

		}
	}

}
