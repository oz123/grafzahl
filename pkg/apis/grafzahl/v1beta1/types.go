package v1beta1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +genclient:nonNamespaced

// Numbers is a specification for a Numbers resource
// if you use plural here the code generated is going to be:
// GrafzahlV1beta1().Numberses().Get(context.TODO(), "total", metav1.GetOptions{})
type Number struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec NumberSpec `json:"spec"`
}

// NumberSpec is the spec for a Numeresource
type NumberSpec struct {
	Pods        int32 `json:"pods"`
	Deployments int32 `json:"deployments"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// NumberList is a list of Number resources
type NumberList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`

	Items []Number `json:"items"`
}
