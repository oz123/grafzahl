import kopf


@kopf.on.delete('pods', labels={'grafzahl': kopf.PRESENT})
def delete_fn(body, name, logger, **kwargs):
    # do decrement of Number here
    print("pod deleted")
    
@kopf.on.create('pods', labels={'grafzahl': kopf.ABSENT})
def create_fn(body, name, namespace, logger, **kwargs):
    # do increment of Number here
    print("pod created")
