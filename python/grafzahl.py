import datetime
import time
import pykube 

from pykube import Pod, Deployment, Event

DEFAULT_SCAN_DELAY=3
SELECTOR = {'grafzahl!': 'counted'}


def gz_update_msg(element, messageType, num):
    return "Normal", messageType, f"{element}: {num}"


class Number(pykube.objects.APIObject):
    version = "grafzahl.io/v1beta1"
    endpoint = "numbers"
    kind = "Number"


class GrafzahlEvent(Event):
    
    # Event still missing From and Age
    event_skel = {"apiVersion": "v1", "kind": "Event","count": 1,
                  "source": {
                    "component": "GrafzahlZahl"
                  },
                  "firstTimestamp": None, # to be filled
                  "involvedObject": None, # to be filled
                  "lastTimestamp": None,
                  "message": None,
                  "reason": None,
                  "type":None,
                  "metadata": {"name": None,},
                  }
    def __init__(self, api):
        
        event = GrafzahlEvent.event_skel.copy()
        event["metadata"]["name"] = "Unintialized"

        super().__init__(api, event)

    def emit(self, objekt, event_type, reason, message):
        
        # python 3.7 and later has time_ns
        # follow the same naming convention as the
        # go client see
        # gh.com/kubernetes/client-go/blob/782ff783b635df54ddf44d55f3bd3e48eb4dcb9a/tools/record/event.go#L366
        self.obj["metadata"]["name"] = "%s.%x" % (objekt["metadata"]["name"], time.time_ns())
        self.obj["involvedObject"] = {"kind": objekt["kind"],
                                      "name": objekt["metadata"]["name"],
                                      "resourceVersion": objekt["metadata"]["resourceVersion"],
                                      "uid": objekt["metadata"]["uid"],
                                      "apiVersion":objekt["apiVersion"]}
        self.obj["lastTimestamp"] = datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
        self.obj["firstTimestamp"] = datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
        self.obj["type"] = event_type
        self.obj["reason"] = reason
        self.obj["message"] = message
        self.create()


def _init(api):
    dx = len(Deployment.objects(api).filter(namespace=pykube.all))
    px = len(Pod.objects(api).filter(namespace=pykube.all))
    numbers = {"deployments": dx, "pods": px}
    # reset total on start
    try:
        total = Number.objects(api).get(name="total")
        total.obj["spec"] = numbers
        total.update()
    except pykube.exceptions.ObjectDoesNotExist:
        total = Number(api, obj=dict(metadata={"name":"total"},
                                     kind="Number",
                                     apiVersion="grafzahl.io/v1beta1",
                                     spec=numbers))
        total.create()
    
    GrafzahlEvent(api).emit(total.obj, "Normal", "GrafzahlReady", "Ready to count")

    return total


def main():
    # automatically detect whether in cluster or load ~/.kube/config
    config = pykube.KubeConfig.from_env()

    api = pykube.HTTPClient(config)
   
    total = _init(api)

    while True:
        for element in [Pod,Deployment]:
            dx = 0
            print("Counting %ss" % element.kind)
            for dx, obj in enumerate(element.objects(api).filter(
                                                 namespace=pykube.all,
                                                 selector=SELECTOR),
                                     start=1):
                print(element.kind, dx, obj)
                print(f'Updating {element.kind} {obj.namespace}/{obj.name}..')
                obj.labels['grafzahl'] = 'counted'
                try:
                    obj.update()
                except pykube.exceptions.HTTPError:
                    obj.reload()
                    obj.labels['grafzahl'] = 'counted'

            element_name =  element.kind.lower() + "s"

            if dx:
                total.obj["spec"][element_name] = total.obj["spec"][element_name] + dx
                GrafzahlEvent(api).emit(total.obj, *gz_update_msg(element_name,
                                                                  "%sAdded" % element_name,
                                                                  total.obj["spec"][element_name]))
                total.update()
            # detect delete
            else:
                print("No new %ss" % element.kind)
                dx = len(element.objects(api).filter(namespace=pykube.all))
                print(element.kind, dx)
                if dx < total.obj["spec"][element_name]:
                    GrafzahlEvent(api).emit(total.obj, *gz_update_msg(element_name, 
                                                                      "%sDeleted" % element_name,
                                                                      total.obj["spec"][element_name]))
                    total.obj["spec"][element_name] = dx
                    total.update()

        time.sleep(DEFAULT_SCAN_DELAY)


if __name__ == "__main__":
    main()
